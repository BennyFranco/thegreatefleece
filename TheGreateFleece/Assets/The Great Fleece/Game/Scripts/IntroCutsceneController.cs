﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroCutsceneController : MonoBehaviour
{
    public Transform finalCameraPosition;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DisableCutScene());
    }

    public IEnumerator DisableCutScene()
    {
        yield return new WaitForSeconds(61.0f);
        ExecuteBeforeEndCinematic();
    }

    public void ExecuteBeforeEndCinematic()
    {
        Camera.main.transform.position = finalCameraPosition.position;
        Camera.main.transform.rotation = finalCameraPosition.rotation;
        Camera.main.fieldOfView = 60.0f;
        AudioManager.Instance.PlayAmbientMusic();
        gameObject.SetActive(false);
    }
}
