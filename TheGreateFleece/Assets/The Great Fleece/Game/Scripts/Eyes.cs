﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eyes : MonoBehaviour
{
    public GameObject gameOverCutscene;
    public bool isCamera;
    public float timeToStartCurScene;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (isCamera)
            {
                // This behaviour is to affect all cameras, but by this time I only want affect current camera
                //GameObject[] securityCameras = GameObject.FindGameObjectsWithTag("CameraCone");
                //foreach (var securityCam in securityCameras)
                //{
                //    var cameraRenderer = securityCam.GetComponent<Renderer>();
                //    Color color = new Color(1.0f, 0.0f, 0.0f, 0.03921569f);
                //    cameraRenderer.material.SetColor("_TintColor", color);                    
                //}

                var cameraRenderer = GetComponent<Renderer>();
                Color color = new Color(1.0f, 0.0f, 0.0f, 0.03921569f);
                cameraRenderer.material.SetColor("_TintColor", color);
                GetComponentInParent<Animator>().enabled = false;
            }
            StartCoroutine(AlertRoutine(timeToStartCurScene));
        }
    }

    IEnumerator AlertRoutine(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        AudioManager.Instance.ambientMusic.Stop();
        gameOverCutscene.SetActive(true);
    }
}
