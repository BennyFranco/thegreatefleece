﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GuardAI : MonoBehaviour
{
    public List<Transform> wayPoints;
    public bool coinTossed;

    private NavMeshAgent agent;
    private Animator animator;

    [SerializeField]
    private int currentTarget;

    private bool reverse;
    private bool targetReached;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (wayPoints.Count > 0 && wayPoints[currentTarget] != null && !coinTossed)
        {
            agent.SetDestination(wayPoints[currentTarget].position);

            if (!IsDistanceGreatherThanStoppingDistance() && targetReached == false)
            {
                targetReached = true;
                StartCoroutine(WaitBeforeMoving());
            }
        }

        if (IsDistanceGreatherThanStoppingDistance())
        {
            targetReached = false;
            animator.SetBool("Walk", true);
        } else if(coinTossed)
        {
            coinTossed = false;
            agent.stoppingDistance = 0;
            currentTarget = 0;
            targetReached = true;
            StartCoroutine(WaitBeforeMoving());
        }
    }

    IEnumerator WaitBeforeMoving()
    {
        float waitTime = IsGuardInALimit() ? Random.Range(2.0f, 5.0f) : 0.0f;
        if (waitTime > 0)
            animator.SetBool("Walk", false);
        yield return new WaitForSeconds(waitTime);

        if (wayPoints.Count > 1)
        {
            if (currentTarget == wayPoints.Count - 1)
            {
                reverse = true;
            }
            else if (currentTarget == 0)
            {
                reverse = false;
            }

            if (reverse)
                currentTarget--;
            else
                currentTarget++;
        }
    }

    bool IsGuardInALimit()
    {
        return (currentTarget == wayPoints.Count - 1 || currentTarget == 0);
    }

    bool IsDistanceGreatherThanStoppingDistance()
    {
        return Vector3.Distance(transform.position, agent.destination) > agent.stoppingDistance + 1;
    }
}
