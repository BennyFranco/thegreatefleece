﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoiceOverTrigger : MonoBehaviour
{
    public AudioClip voiceOver;

    private bool wasTriggered;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !wasTriggered)
        {
            AudioManager.Instance.PlayVoiceOver(voiceOver);
            wasTriggered = true;
        }
    }
}
