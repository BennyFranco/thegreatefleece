﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    private static AudioManager instance;
    public static AudioManager Instance
    {
        get
        {
            if (instance == null)
            {
                Debug.LogError("AudioManager is null!");
            }

            return instance;
        }
    }

    public AudioSource voiceOver;
    public AudioSource ambientMusic;

    private void Awake()
    {
        instance = this;
    }

    public void PlayVoiceOver(AudioClip clip)
    {
        voiceOver.clip = clip;
        voiceOver.Play();
    }

    public void PlayAmbientMusic()
    {
        ambientMusic.gameObject.SetActive(true);
    }
}
