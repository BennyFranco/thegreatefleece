﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinStateActivation : MonoBehaviour
{
    public GameObject cutscene;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (GameManager.Instance.HasCard)
            {
                AudioManager.Instance.ambientMusic.Stop();
                cutscene.SetActive(true);
            }
            else
            {
                Debug.Log("NEEDS A CARD!");
            }
        }
    }
}
