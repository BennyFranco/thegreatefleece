﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                Debug.LogError("GameManager is null!");
            }

            return instance;
        }
    }

    public bool HasCard { get; set; }
    public GameObject introCutscene;
    public GameObject player;

    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            SkipIntroCutScene();
        }
    }

    private void SkipIntroCutScene()
    {
        PlayableDirector director = introCutscene.GetComponent<PlayableDirector>();
        IntroCutsceneController controller = introCutscene.GetComponent<IntroCutsceneController>();
        controller.StopAllCoroutines();
        director.time = 50.0f;
        controller.ExecuteBeforeEndCinematic();
        player.SetActive(true);
    }
}
