﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabKeyCardActivation : MonoBehaviour
{
    public GameObject sleepingGuardCutscene;
    public Transform cameraFinalPosition;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            sleepingGuardCutscene.SetActive(true);
            GameManager.Instance.HasCard = true;
            StartCoroutine(DisableCutScene());
        }
    }

    IEnumerator DisableCutScene()
    {
        yield return new WaitForSeconds(5.1f);
        Camera.main.transform.position = cameraFinalPosition.position;
        Camera.main.transform.rotation = cameraFinalPosition.rotation;
        sleepingGuardCutscene.SetActive(false);
    }
}
