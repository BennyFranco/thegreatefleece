﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Player : MonoBehaviour
{
    public GameObject coinPrefab;
    public AudioClip coinSoundEffect;

    private NavMeshAgent agent;
    private Animator animator;
    private Vector3 target;
    private bool coinTossed;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                target = hit.point;
                agent.SetDestination(target);
                animator.SetBool("Walk", true);

            }
        }

        if (Input.GetMouseButtonDown(1) && coinTossed == false)
        {
            Ray rayOrigin = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(rayOrigin, out RaycastHit hit))
            {
                coinTossed = true;
                animator.SetTrigger("Throw");
                Instantiate(coinPrefab, hit.point, Quaternion.identity);
                AudioSource.PlayClipAtPoint(coinSoundEffect, hit.point, 1.0f);
                SendAIToCoinSpot(hit.point);
            }
        }

        if (Vector3.Distance(transform.position, target) < 1)
        {
            animator.SetBool("Walk", false);
        }
    }

    void SendAIToCoinSpot(Vector3 point)
    {
        GameObject[] guards = GameObject.FindGameObjectsWithTag("Guard1");
        foreach (var guard in guards)
        {
            GuardAI ai = guard.GetComponent<GuardAI>();
            ai.coinTossed = coinTossed;

            NavMeshAgent currentGuardAgent = guard.GetComponent<NavMeshAgent>();
            currentGuardAgent.stoppingDistance = Random.Range(0, 10);
            currentGuardAgent.SetDestination(point);
        }
    }
}
